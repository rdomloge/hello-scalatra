FROM tomcat
MAINTAINER Ramsay Domloge
COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war
EXPOSE 8080
